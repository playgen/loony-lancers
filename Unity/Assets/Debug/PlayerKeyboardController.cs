﻿using PlayGen.LoonyLancers.Assets.Scripts;
using UnityEngine;

namespace PlayGen.LoonyLancers.Assets.Debug
{
    public class PlayerKeyboardController : MonoBehaviour 
    {
        public int Id;

        // Update is called once per frame
        void Update () 
        {
            Vector2 motion = Vector2.zero;

            if (Input.GetKey (KeyCode.UpArrow)) 
            {
                motion.y += 1;
            }

            if (Input.GetKey (KeyCode.DownArrow)) 
            {
                motion.y += -1;
            }

            if (Input.GetKey (KeyCode.LeftArrow)) 
            {
                motion.x += -1;
            }

            if (Input.GetKey (KeyCode.RightArrow)) 
            {
                motion.x += 1;
            }

            Orchestrator.Instance.ProcessPlayerMotion (Id, motion);

            if (Input.GetKeyDown(KeyCode.Space))
            {
                Orchestrator.Instance.ProcessPlayerButtonPress(Id, "attack");
            }
        }
    }
}
