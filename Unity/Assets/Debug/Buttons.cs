﻿using PlayGen.LoonyLancers.Assets.Scripts;
using UnityEngine;

namespace PlayGen.LoonyLancers.Assets.Debug
{
    public class Buttons : MonoBehaviour
    {
        private void OnGUI()
        {
            if (GUILayout.Button("Add Keyboard Player"))
            {
                var id = int.MaxValue;
                var player = Orchestrator.Instance.AddPlayer(id);
                var controller = player.gameObject.AddComponent<PlayerKeyboardController>();
                controller.Id = id;
            }
        }
    }
}
