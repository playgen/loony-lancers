﻿using UnityEngine;

namespace PlayGen.LoonyLancers.Assets.Scripts.Castles
{
    public class CastleSpwanerBehaviour : MonoBehaviour
    {
        [SerializeField]
        private RectTransform _parent;

        [SerializeField]
        private CastleBehaviour[] _castleTypes;

        private int _index;

        public CastleBehaviour Spawn(float startWeight, float decreaseWeight, float goldLimit, float targetWeight)
        {
            var castle = Instantiate(_castleTypes[_index], _parent);
            castle.Weight = startWeight;
            castle.DecreaseWeight = decreaseWeight;
            castle.Limit = goldLimit;
            castle.TargetWeight = targetWeight;

            _index++;
            _index %= _castleTypes.Length;

            return castle;
        }
    }
}
