﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace PlayGen.LoonyLancers.Assets.Scripts.Castles
{
    public class CastleBehaviour : MonoBehaviour
    {
        [SerializeField]
        private Slider _slider;

        [SerializeField]
        private Text _amount;
	
        public float DecreaseWeight { get; set; }

        public float TargetWeight { get; set; }

        public float Weight
        {
            get { return _slider.value; }
            set
            {
                var previousWeight = _slider.value;

                _slider.value = value;

                CheckWeight(previousWeight);

                _amount.text = Mathf.CeilToInt(Value).ToString();
            }
        }

        public float Value
        {
            get { return Weight * Limit; }
            set
            {
                var difference = value - Value;
                var additionalWeight = difference / Limit;
                Weight += additionalWeight;
            }
        }

        public bool WillKill(float value)
        {
            return value <= 0 || Limit <= value;
        }

        private void CheckWeight(float previousWeight)
        {
            if (Weight >= 1)
            {
                FilledEvent();
            }
            else if (Weight <= 0)
            {
                EmptyEvent();
            }
            else if (previousWeight < TargetWeight && Weight >= TargetWeight)
            {
                TargetEvent();
            }
        }

        public float Limit { get; set; }

        public bool IsInTargetWeight
        {
            get { return Weight > TargetWeight && Weight < 1; }
        }

        public event Action EmptyEvent;

        public event Action FilledEvent;

        public event Action TargetEvent;

        public void Update()
        {
            Weight -= DecreaseWeight * Time.deltaTime;
        }
    }
}
