﻿using System;
using System.Collections.Generic;
using System.Linq;
using NDream.AirConsole;
using Newtonsoft.Json.Linq;
using PlayGen.LoonyLancers.Assets.Scripts.Balloons;
using PlayGen.LoonyLancers.Assets.Scripts.Castles;
using PlayGen.LoonyLancers.Assets.Scripts.Messages;
using PlayGen.LoonyLancers.Assets.Scripts.Players;
using PlayGen.LoonyLancers.Assets.Scripts.Stats;
using UnityEngine;
using UnityEngine.UI;

namespace PlayGen.LoonyLancers.Assets.Scripts
{
    public class Orchestrator : MonoBehaviour
    {
        [SerializeField]
        private int _startingLevelIndex;

        [SerializeField]
        private List<LevelConfig> _levelConfigs;
        [SerializeField]
        private PlayerSpawnerBehaviour _playerSpwaner;
        [SerializeField]
        private CastleSpwanerBehaviour _castleSpwaner;
        [SerializeField]
        private RectTransform _playingField;
        [SerializeField]
        private BalloonSpawnerBehaviour[] _balloonSpawners;
        [SerializeField]
        private StatManagerBehaviour _statManager;

        [SerializeField] private GameObject _wonText;
        [SerializeField] private GameObject _lostText;

        [SerializeField] public SwitchupController[] _switchupControllers;

        [SerializeField] private GameObject _levelStartImage;
        [SerializeField] private Text _levelStartText;

        [SerializeField] private AudioSource _baloonPop;
        [SerializeField] private AudioSource _poke;

        [SerializeField] private List<Text> _targetScore;
        [SerializeField] private List<Text> _scoreLimit;

        public Dictionary<int, Vector2> YOrientationMappings { get; set; }
        public Dictionary<int, Vector2> XOrientationMappings { get; set; }

        public Dictionary<int, Vector2> YOrientationMappings_Default = new Dictionary<int, Vector2>()
        {
            {-1, new Vector2(0, 1)},
            {0, new Vector2(0, 1)}
        };

        public Dictionary<int, Vector2> XOrientationMappings_Default = new Dictionary<int, Vector2>()
        {
            {-1, new Vector2(1, 0)},
            {0, new Vector2(1, 0)}
        };

        private int _currentLevel;

        private readonly List<BalloonBehaviour> _balloons = new List<BalloonBehaviour>();
        public readonly List<PlayerBehaviour> Players = new List<PlayerBehaviour>();
        public readonly List<CastleBehaviour> Castles = new List<CastleBehaviour>();
        public Dictionary<int, PlayerBehaviour> DevicePlayerMappings = new Dictionary<int, PlayerBehaviour>();
        public readonly Dictionary<PlayerBehaviour, CastleBehaviour> _playerCastleMappings = new Dictionary<PlayerBehaviour, CastleBehaviour>();
        private bool _isPaused;

        public static Orchestrator Instance { get; private set; }

        public int CurrentLevel
        {
            get { return _currentLevel; }
        }

        private LevelConfig CurrentLevelConfig
        {
            get { return _levelConfigs[_currentLevel]; }
        }

        private void Awake()
        {
            Instance = this;

            if (AirConsole.instance != null)
            {
                AirConsole.instance.onMessage += OnMessage;
                AirConsole.instance.onConnect += id => AddPlayer(id);
                AirConsole.instance.onDisconnect += RemovePlayer;
            }
        }

        private void RemovePlayer(int deviceId)
        {
            var player = DevicePlayerMappings[deviceId];
            DevicePlayerMappings.Remove(deviceId);

            Players.Remove(player);
            var castle = _playerCastleMappings[player];
            Castles.Remove(castle);

            Destroy(castle.gameObject);
            Destroy(player.gameObject);
        }

        private void Start()
        {
            StartLevel(_startingLevelIndex);
        }

        private void StartLevel(int level)
        {
            _wonText.SetActive(false);
            _lostText.SetActive(false);

            Castles.ForEach(castle => castle.enabled = true);
            Players.ForEach(p => p.enabled = true);
            _balloons.ForEach(b => b.enabled = true);

            YOrientationMappings = YOrientationMappings_Default;
            XOrientationMappings = XOrientationMappings_Default;

            _currentLevel = level;

            _levelStartText.text = "Level " + (_currentLevel + 1);
            _levelStartImage.SetActive(true);

            _balloons.ForEach(b => Destroy(b.gameObject));
            _balloons.Clear();

            for (var i = 0; i < CurrentLevelConfig.BalloonCount; i++)
            {
                var balloon = _balloonSpawners[_currentLevel].Spawn(_playingField);
                _balloons.Add(balloon);
            }

            _statManager.SetTargetWeight(CurrentLevelConfig.GoldTargetWeight);

            Castles.ForEach(castle =>
            {
                castle.Weight = CurrentLevelConfig.StartingGoldWeight;
                castle.DecreaseWeight = CurrentLevelConfig.GoldDecreaseWeight;
                castle.Limit = CurrentLevelConfig.GoldLimit;
                castle.TargetWeight = CurrentLevelConfig.GoldTargetWeight;
            });

            foreach (var switchupController in _switchupControllers)
            {
                switchupController.gameObject.SetActive(false);
            }

            _balloonSpawners[_currentLevel].Reset();
            _switchupControllers[_currentLevel].Reset();

            _targetScore.ForEach(t => t.text = Mathf.CeilToInt(CurrentLevelConfig.GoldTargetWeight * CurrentLevelConfig.GoldLimit).ToString());
            _scoreLimit.ForEach(t => t.text = CurrentLevelConfig.GoldLimit.ToString());


            Invoke("ResumeLevel", 2);
        }

        void ResumeLevel()
        {
            Castles.ForEach(c => c.gameObject.SetActive(true));
            _switchupControllers[_currentLevel].gameObject.SetActive(true);
            _levelStartImage.SetActive(false);
            _isPaused = false;
        }

        void Pause()
        {
            _isPaused = true;
            Castles.ForEach(castle => castle.enabled = false);
            Players.ForEach(p => p.enabled = false);
            _balloons.ForEach(b => b.enabled = false);

            foreach (var balloonSpawner in _balloonSpawners)
            {
                balloonSpawner.gameObject.SetActive(false);
            }
        }

        public PlayerBehaviour AddPlayer(int device_id)
        {
            var player = _playerSpwaner.Spawn(_playingField, CurrentLevelConfig.PlayerMaxSpeed);
            var castle = _castleSpwaner.Spawn(CurrentLevelConfig.StartingGoldWeight, CurrentLevelConfig.GoldDecreaseWeight, CurrentLevelConfig.GoldLimit, CurrentLevelConfig.GoldTargetWeight);
            castle.EmptyEvent += CastleOnEmpty;
            castle.FilledEvent += CastleOnFilled;
            castle.TargetEvent += CastleOnTarget;
        
            Players.Add(player);
            Castles.Add(castle);

            DevicePlayerMappings[device_id] = player;
            _playerCastleMappings[player] = castle;

            return player;
        }

        private void CastleOnFilled()
        {
            Pause();
            _lostText.SetActive(true);

            Invoke("RestartFirstLevel", 5);
        }

        private void CastleOnEmpty()
        {
            Pause();
            _lostText.SetActive(true);

            Invoke("RestartFirstLevel", 5);
        }

        private void RestartFirstLevel()
        {
            StartLevel(0);
        }

        private void CastleOnTarget()
        {
            if (Castles.All(c => c.IsInTargetWeight))
            {
                Pause();

                _wonText.SetActive(true);

                Invoke("StartNextLevel", 5);
            }
        }

        private void StartNextLevel()
        {
            var nextLevel = _currentLevel + 1;
            nextLevel %= _levelConfigs.Count;
            StartLevel(nextLevel);
        }

        private void OnMessage(int from, JToken data)
        {
            var message = data.ToObject<Message>();
            if (message != null)
            {
                switch (message.Type)
                {
                    case "buttonPress":
                        var buttonPress = data.ToObject<ButtonPress>();
                        ProcessPlayerButtonPress(from, buttonPress.Button);
                        break;

                    case "motion":
                        var motion = data.ToObject<Messages.Motion>();
                        ProcessPlayerMotion(from, motion.MotionVector);
                        break;

                    case "log":
                        var log = data.ToObject<Log>();
                        UnityEngine.Debug.Log("Message from: " + from);
                        UnityEngine.Debug.Log(log.Message);
                        break;

                    default:
                        throw new ArgumentException("Unhandled case: " + message.Type);
                }
            }
        }

        public void ProcessPlayerButtonPress(int from, string buttonPress)
        {
            switch (buttonPress)
            {
                case "attack":
                    ProcessPlayerAttack(from);
                    break;

                default:
                    throw new ArgumentException("Unhandled case: " + buttonPress);
            }
        }

        private void ProcessPlayerAttack(int from)
        {
            if (_isPaused) return;

            var player = DevicePlayerMappings[from];
            player.Attack();
            _poke.Play();

            var castle = _playerCastleMappings[player];

            var intersection = _balloons.FirstOrDefault(
                b => b.GetComponent<Collider2D>().OverlapPoint(player.Head.position));

            if(intersection != null)
            {
                _balloons.Remove(intersection);
                intersection.OnPop(castle);

                _baloonPop.Play();

                var balloon = _balloonSpawners[_currentLevel].Spawn(_playingField);
                _balloons.Add(balloon);
            }
        }

        public void ProcessPlayerMotion(int from, Vector2 motion)
        {
            if (_isPaused) return;

            var player = DevicePlayerMappings[from];

            motion = ApplyOrientation(motion);

            player.TargetDirection = motion;
        }

        private Vector2 ApplyOrientation(Vector2 motion)
        {
            var xSign = Math.Sign(motion.x);
            var ySign = Math.Sign(motion.y);

            xSign = xSign == 1 ? 0 : xSign;
            ySign = ySign == 1 ? 0 : ySign;
            
            var changedMotion = Vector2.zero;
            changedMotion += motion.x * XOrientationMappings[xSign];
            changedMotion += motion.y * YOrientationMappings[ySign];

            return changedMotion;
        }
    }
}
