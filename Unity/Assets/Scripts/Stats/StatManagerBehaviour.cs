﻿using UnityEngine;

namespace PlayGen.LoonyLancers.Assets.Scripts.Stats
{
    public class StatManagerBehaviour : MonoBehaviour
    {
        [SerializeField]
        private RectTransform _target;
        [SerializeField]
        private RectTransform _statContainer;

        public void SetTargetWeight(float goldTargetWeight)
        {
            var length = _statContainer.rect.yMax - _statContainer.rect.yMin;
            var offset = length * goldTargetWeight;
            offset -= length / 2;

            var position = _target.localPosition;
            position.y = offset;
            _target.localPosition = position;
        }
    }
}
