﻿using UnityEngine;

namespace PlayGen.LoonyLancers.Assets.Scripts.Messages
{
	public class Motion : Message
	{
		public Vector2 MotionVector { get; set; }
	}
}