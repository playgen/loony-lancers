﻿namespace PlayGen.LoonyLancers.Assets.Scripts.Messages
{
    public class ButtonPress : Message
    {
        public string Button { get; set; }
    }
}
