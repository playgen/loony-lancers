﻿using System;
using UnityEngine;

namespace PlayGen.LoonyLancers.Assets.Scripts
{
    [Serializable]
    public class LevelConfig
    {
        [SerializeField]
        public int BalloonCount;

        [SerializeField]
        [Range(0, 1)]
        public float StartingGoldWeight;

        [SerializeField]
        [Range(0, 1)]
        public float GoldDecreaseWeight;

        [SerializeField]
        [Range(0, 1)]
        public float GoldTargetWeight;

        [SerializeField]
        public float PlayerMaxSpeed;

        [SerializeField]
        public float GoldLimit;
    }
}
