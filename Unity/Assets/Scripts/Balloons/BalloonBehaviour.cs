﻿using PlayGen.LoonyLancers.Assets.Scripts.Castles;
using UnityEngine;

namespace PlayGen.LoonyLancers.Assets.Scripts.Balloons
{
    [RequireComponent(typeof(EffectBehaviour))]
    public class BalloonBehaviour : MonoBehaviour
    {
        [SerializeField]
        private GameObject[] _spawnOnDead;

        public bool CanStart { get { return Effect.CanStart; } }

        private EffectBehaviour Effect
        {
            get
            {
                return GetComponent<EffectBehaviour>();
            }
        }

        public void OnPop(CastleBehaviour castle)
        {
            foreach (var toSpawn in _spawnOnDead)
            {
                var spawned = Instantiate(toSpawn, transform.parent);
                spawned.transform.localPosition = transform.localPosition;
            }

            Effect.Apply(castle);

            Effect.CrerateText();

            Destroy(gameObject);
        }

        public bool WillKill(CastleBehaviour castle)
        {
            return Effect.WillKill(castle);
        }
    }
}
