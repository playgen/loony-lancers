﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlayGen.LoonyLancers.Assets.Scripts.Balloons
{
    [Serializable]
    public class SpawnConfig
    {
        [SerializeField]
        public int SpawnFrequency;

        [SerializeField]
        public BalloonBehaviour Balloon;
    }
}