﻿using PlayGen.LoonyLancers.Assets.Scripts.Castles;
using UnityEngine;
using UnityEngine.UI;

namespace PlayGen.LoonyLancers.Assets.Scripts.Balloons.Effects
{
    public class AddBehaviour : EffectBehaviour
    {
        [SerializeField]
        private int _amount;

        public override bool CanStart
        {
            get { return _amount > 0; }
        }

        public override Text CrerateText()
        {
            var text = base.CrerateText();
            text.text = _amount >= 0
                ? "+"
                : string.Empty;

            text.text += _amount;

            return text;
        }

        public override void Apply(CastleBehaviour castle)
        {
            castle.Value += _amount;
        }

        public override bool WillKill(CastleBehaviour castle)
        {
            return castle.WillKill(castle.Value + _amount);
        }
    }
}
