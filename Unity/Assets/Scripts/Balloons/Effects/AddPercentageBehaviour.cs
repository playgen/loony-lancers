﻿using PlayGen.LoonyLancers.Assets.Scripts.Castles;
using UnityEngine;
using UnityEngine.UI;

namespace PlayGen.LoonyLancers.Assets.Scripts.Balloons.Effects
{
    public class AddPercentageBehaviour : EffectBehaviour {

        [SerializeField]
        private int _amount;

        public override bool CanStart
        {
            get { return false; }
        }

        public override Text CrerateText()
        {
            var text = base.CrerateText();
            text.text = _amount >= 0
                ? "+"
                : string.Empty;

            text.text += _amount + "%";

            return text;
        }

        public override void Apply(CastleBehaviour castle)
        {
            castle.Value += _amount * (castle.Value / 100);
        }

        public override bool WillKill(CastleBehaviour castle)
        {
            return castle.WillKill(castle.Value + _amount * (castle.Value / 100)) || castle.Value == 0;
        }
    }
}
