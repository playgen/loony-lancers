﻿using PlayGen.LoonyLancers.Assets.Scripts.Castles;
using UnityEngine;
using UnityEngine.UI;

namespace PlayGen.LoonyLancers.Assets.Scripts.Balloons
{
	public abstract class EffectBehaviour : MonoBehaviour
	{
        [SerializeField]
        protected Text _effectText;

	    public abstract bool CanStart { get; }

	    public virtual Text CrerateText()
	    {
	        var text = Instantiate(_effectText, transform.parent);
	        var parentRect = (RectTransform)transform.parent;
	        var pos = Vector3.zero;
	        pos.x -= parentRect.sizeDelta.x / 2;
            text.transform.localPosition = pos;

	        text.color = GetComponent<Image>().color;

	        return text;
	    }

	    public abstract void Apply(CastleBehaviour castle);

	    public abstract bool WillKill(CastleBehaviour castle);
	}
}
