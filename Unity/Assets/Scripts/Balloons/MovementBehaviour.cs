﻿using UnityEngine;

namespace PlayGen.LoonyLancers.Assets.Scripts.Balloons
{
	public class MovementBehaviour : MonoBehaviour
	{
		[SerializeField]
		private float _min;
		[SerializeField]
		private float _max;
		[SerializeField]
		private float _speedMin;
		[SerializeField]
		private float _speedMax;
		[SerializeField]
		private AnimationCurve _easing;

		private Vector3 _anchorPosition;
		private Vector3 _targetPosition;
		private float _weight;
		private float _weightIncrement;
		private Vector3 _startPosition;

		private void Start()
		{
			_anchorPosition = transform.localPosition;
			Retarget();
		}

		private void Update()
		{
			_weight += _weightIncrement * Time.deltaTime;
			var easedWeight = _easing.Evaluate(_weight);
			transform.localPosition = Vector3.Lerp(_startPosition, _targetPosition, easedWeight);

			if (_weight >= 1)
			{
				Retarget();
			}
		}

		private void Retarget()
		{
			_startPosition = transform.localPosition;
			_targetPosition = CalculateTarget();
			_weight = 0;
			_weightIncrement = 1 / CalculateWeightIncrement(_startPosition, _targetPosition);
		}

		private float CalculateWeightIncrement(Vector3 startPosition, Vector3 targetPosition)
		{
			var totalDistance = Vector3.Distance(startPosition, targetPosition);
			var speed = Random.Range(_speedMin, _speedMax);
			return totalDistance / speed;
		}

		private Vector3 CalculateTarget()
		{
			var range = Random.Range(_min, _max);
			var targetPosition = Random.insideUnitCircle * range;
			targetPosition = (Vector3)targetPosition + _anchorPosition;

			return targetPosition;
		}
	}
}
