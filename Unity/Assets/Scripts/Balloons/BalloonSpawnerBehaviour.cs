﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace PlayGen.LoonyLancers.Assets.Scripts.Balloons
{
    public class BalloonSpawnerBehaviour : MonoBehaviour
    {
        [SerializeField]
        private SpawnConfig[] _spawnConfigs;

        private List<BalloonBehaviour> _balloonPool = new List<BalloonBehaviour>();

        public BalloonBehaviour Spawn(RectTransform parent)
        {
            var balloonRef = _balloonPool.FirstOrDefault(IsViableOption);

            if (balloonRef == null)
            {
                _balloonPool = GenerateBalloonPool();
            }

            balloonRef = _balloonPool.FirstOrDefault(IsViableOption);

            if (balloonRef == null)
            {
                throw new Exception("No viable balloon options available to spawn.");
            }
            else
            {
                _balloonPool.Remove(balloonRef);
            }

            var balloon = Instantiate(balloonRef, parent);
            balloon.transform.SetAsFirstSibling();

            balloon.transform.localPosition = new Vector3()
            {
                x = Random.Range(parent.rect.xMin, parent.rect.xMax),
                y = Random.Range(parent.rect.yMin, parent.rect.yMax),
                z = 0
            };

            return balloon;
        }

        private List<BalloonBehaviour> GenerateBalloonPool()
        {
            var balloons = new List<BalloonBehaviour>();

            foreach (var spawnConfig in _spawnConfigs)
            {
                for (var i = 0; i < spawnConfig.SpawnFrequency; i++)
                {
                    balloons.Add(spawnConfig.Balloon);
                }
            }

            var random = new System.Random();
            balloons.Sort((x, y) => random.Next(-1, 1));

            return balloons;
        }

        private bool IsViableOption(BalloonBehaviour balloon)
        {
            if (!Orchestrator.Instance.Castles.Any())
            {
                return balloon.CanStart;
            }
            else
            {
                return !Orchestrator.Instance.Castles.Any(balloon.WillKill);
            }
        }

        public void Reset()
        {
            _balloonPool.Clear();
        }
    }
}
