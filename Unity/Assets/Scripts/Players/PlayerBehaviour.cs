﻿using UnityEngine;

namespace PlayGen.LoonyLancers.Assets.Scripts.Players
{
    public class PlayerBehaviour : MonoBehaviour
    {
        [SerializeField] private UnityEngine.Animation _animation;
        [SerializeField] private RectTransform _head;

        [HideInInspector]
        public Vector2 TargetDirection = Vector2.zero;
	
        private Rigidbody2D _rigidbody;

        public float MaxSpeed { get; set; }

        public float MaxSpeedTime { get; set; }

        public RectTransform Head {  get { return _head; } }

        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody2D>();
        }

        private void Update()
        {
            _rigidbody.velocity = TargetDirection * MaxSpeed;
        }

        public void Attack()
        {
            _animation.Play();
        }
    }
}
