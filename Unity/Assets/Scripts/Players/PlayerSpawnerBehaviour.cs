﻿using UnityEngine;

namespace PlayGen.LoonyLancers.Assets.Scripts.Players
{
    public class PlayerSpawnerBehaviour : MonoBehaviour
    {
        [SerializeField]
        private PlayerBehaviour[] _playerTypes;

        private int _index;
	
        public PlayerBehaviour Spawn(RectTransform parent, float playerMaxSpeed)
        {
            var player = Instantiate(_playerTypes[_index], parent);
            player.transform.SetAsLastSibling();
            player.MaxSpeed = playerMaxSpeed;

            var playerRect = (RectTransform) player.transform;

            _index++;
            _index %= _playerTypes.Length;

            player.transform.localPosition = new Vector3()
            {
                x = Random.Range(parent.rect.xMin - playerRect.rect.xMin, parent.rect.xMax - playerRect.rect.xMax),
                y = Random.Range(parent.rect.yMin - playerRect.rect.yMin , parent.rect.yMax - playerRect.rect.yMax),
                z = 0
            };

            return player;
        }
    }
}
