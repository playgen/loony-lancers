﻿using System.Collections;
using System.Collections.Generic;
using PlayGen.LoonyLancers.Assets.Scripts;
using UnityEngine;

public class ControlOrientation : Switchup
{
    private static readonly List<Dictionary<string, Dictionary<int, Vector2>>> Orientations = new List<Dictionary<string, Dictionary<int, Vector2>>>()
    {
        // horizontal, vertical swap
        new Dictionary<string, Dictionary<int, Vector2>>()
        {
            {
                "x",
                new Dictionary<int, Vector2>()
                {
                    {-1, new Vector2(0, 1)},
                    {0, new Vector2(0, 1)}
                }
            },
            {
                "y",
                new Dictionary<int, Vector2>()
                {
                    {-1, new Vector2(1, 0)},
                    {0, new Vector2(1, 0)}
                }
            }
        },
        //invert
        new Dictionary<string, Dictionary<int, Vector2>>()
        {
            {
                "x",
                new Dictionary<int, Vector2>()
                {
                    { -1, new Vector2(-1, 0)},
                    {0, new Vector2(-1, 0)}
                }
            },
            {
                "y",
                new Dictionary<int, Vector2>()
                {
                    {-1, new Vector2(0, -1)},
                    {0, new Vector2(0, -1)}
                }
            }
        },
        // normal
        new Dictionary<string, Dictionary<int, Vector2>>()
        {
            {
                "x",
                new Dictionary<int, Vector2>()
                {
                    {-1, new Vector2(1, 0)},
                    {0, new Vector2(1, 0)}
                }
            },
            {
                "y",
                new Dictionary<int, Vector2>()
                {
                    {-1, new Vector2(0, 1)},
                    {0, new Vector2(0, 1)}
                }
            }
        },
    };

    public override void Apply()
    {
        var orientation = Orientations[Random.Range(0, Orientations.Count)];
        Orchestrator.Instance.XOrientationMappings = orientation["x"];
        Orchestrator.Instance.YOrientationMappings = orientation["y"];
    }
}
