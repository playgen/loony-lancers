﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class SwitchupFrequencyConfig
{
    [SerializeField]
    public int Frequency;

    [SerializeField]
    public string SwithcupType;
}
