﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using PlayGen.LoonyLancers.Assets.Scripts;
using PlayGen.LoonyLancers.Assets.Scripts.Players;
using UnityEngine;

public class Players : Switchup
{
    public override void Apply()
    {
        var direction = Random.Range(-1, 0);
        direction = direction == 0 ? direction : 1;

        var devices = new List<int>();
        var players = new List<PlayerBehaviour>();

        foreach (var mapping in Orchestrator.Instance.DevicePlayerMappings)
        {
            devices.Add(mapping.Key);
            players.Add(mapping.Value);
        }

        if (direction < 0)
        {
            var device = devices[0];
            devices.Remove(device);
            devices.Add(device);
        }
        else
        {
            var device = devices.Last();
            devices.Remove(device);
            devices.Insert(0, device);
        }

        var mappings = new Dictionary<int, PlayerBehaviour>();

        for (var i = 0; i < devices.Count; i++)
        {
            mappings.Add(devices[i], players[i]);
        }

        Orchestrator.Instance.DevicePlayerMappings = mappings;
    }
}
