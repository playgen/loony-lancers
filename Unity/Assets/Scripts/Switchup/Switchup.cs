﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Switchup
{
    public abstract void Apply();
}
