﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class SwitchupController : MonoBehaviour
{
    [SerializeField]
    private SwitchupFrequencyConfig[] _swithcups;

    [SerializeField]
    private float _switchupInterval;

    [SerializeField]
    private float _switchupStartWait;

    [SerializeField]
    private Text _switchupTimer;

    [SerializeField] private Animation _onSwitchanimation;

    private float _sinceEnabledElapsedTime;
    private float _elapsedTime;

    private List<string> _switchupPool = new List<string>();
    
    public void Update()
    {
        float remainingTime;
        if (_sinceEnabledElapsedTime < _switchupStartWait)
        {
            remainingTime = _switchupStartWait - _sinceEnabledElapsedTime;
        }
        else
        {
            remainingTime = _switchupInterval - _elapsedTime;
        }

        _switchupTimer.text = Mathf.CeilToInt(remainingTime).ToString();

        if (_elapsedTime > _switchupInterval && _sinceEnabledElapsedTime > _switchupStartWait)
        {
            _elapsedTime = 0;

            DoSwitchup();
        }

        _elapsedTime += Time.deltaTime;
        _sinceEnabledElapsedTime += Time.deltaTime;
    }

    public void DoSwitchup()
    {
        _onSwitchanimation.Play();

        if (!_switchupPool.Any())
        {
            _switchupPool = GenerateSwitchupPool();
        }

        if (!_switchupPool.Any())
        {
            throw new Exception("No viable switchup options available to apply.");
        }

        var switchupType = _switchupPool[0];
        _switchupPool.Remove(switchupType);

        ExecuteType(switchupType);
    }

    private List<string> GenerateSwitchupPool()
    {
        var switchups = new List<string>();

        foreach (var spawnConfig in _swithcups)
        {
            for (var i = 0; i < spawnConfig.Frequency; i++)
            {
                switchups.Add(spawnConfig.SwithcupType);
            }
        }

        var random = new System.Random();
        switchups.Sort((x, y) => random.Next(-1, 1));

        return switchups;
    }

    private void ExecuteType(string switchupType)
    {
        Switchup switchup;

        switch (switchupType)
        {
            case "ControlOrientation":
                switchup = new ControlOrientation();
                break;

            case "Players":
                switchup = new Players();
                    break;

            default:
                throw new ArgumentException("Unhandled case " + switchupType);
        }

        switchup.Apply();
    }

    private void OnEnable()
    {
        _elapsedTime = 0;
        _sinceEnabledElapsedTime = 0;
    }

    public void Reset()
    {
        _switchupPool.Clear();
    }
}
