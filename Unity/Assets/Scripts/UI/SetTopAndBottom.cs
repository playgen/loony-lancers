﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class SetTopAndBottom : MonoBehaviour
{
    [SerializeField]
    private RectTransform _top;

    [SerializeField]
    private RectTransform _bottom;
    
	// Update is called once per frame
	void Update ()
	{
	    var trans = (RectTransform) transform;
	    var size = trans.sizeDelta;
	    size.y = _top.localPosition.y - _bottom.localPosition.y;

	    trans.sizeDelta = size;

	    var pos = trans.localPosition;
	    pos.y = _bottom.localPosition.y + (size.y / 2);

	    trans.localPosition = pos;
	}
}
