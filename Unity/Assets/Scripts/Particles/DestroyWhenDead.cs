﻿using UnityEngine;

namespace PlayGen.LoonyLancers.Assets.Scripts.Particles
{
    [RequireComponent(typeof(ParticleSystem))]
    public class DestroyWhenDead : MonoBehaviour
    {
        private ParticleSystem _particles;

        private void Awake ()
        {
            _particles = GetComponent<ParticleSystem>();
        }
	
        void Update ()
        {
            if (!_particles.IsAlive())
            {
                Destroy(gameObject);
            }
        }
    }
}
