﻿using UnityEngine;

namespace PlayGen.LoonyLancers.Assets.Scripts.Animation
{
    [RequireComponent(typeof(UnityEngine.Animation))]
    public class DestroyWhenDone : MonoBehaviour
    {
        private UnityEngine.Animation _animation;

        private void Awake()
        {
            _animation = GetComponent<UnityEngine.Animation>();
        }

        // Update is called once per frame
        void Update ()
        {
            if (!_animation.isPlaying)
            {
                Destroy(gameObject);
            }
        }
    }
}
